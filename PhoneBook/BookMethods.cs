﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PhoneBook
{
    public class BookMethods
    {

        public void ListContacts(List<PhoneNumber> numbers)
        {
            Console.Clear();
            var orderedNumbers = numbers.OrderBy(o => o.Name);
            Console.WriteLine("List of available contacts:");
            foreach (var n in orderedNumbers)
            {
                Console.WriteLine($"Contact:\n\tName: {n.Name}\n\tNumber: {n.Number}");
                if (n.Note != null)
                {
                    Console.WriteLine($"\tNote: {n.Note}");
                }
            }
            Console.ReadKey();
        }

        public PhoneNumber FindContact(List<PhoneNumber> numbers, string name)
        {
            var contact = numbers.Find(n => n.Name.ToLower() == name);
            if (contact == null)
            {
                Console.WriteLine("Specified contact can't be found.");
                return null;
            }
            return contact;
        }

        public void FindAndPrintContact(List<PhoneNumber> numbers, string name)
        {
            var contact = FindContact(numbers, name);
            if (contact != null)
            {
                Console.WriteLine($"Contact details:\n\tName: {contact.Name}\n\tNumber: {contact.Number}");
                if (contact.Note != null)
                {
                    Console.WriteLine($"\tNote: {contact.Note}");
                }
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Just a helper method to avoid console actions, so unit tests can be used.
        /// </summary>
        /// <returns></returns>
        public Tuple<string, int> AddContactValidator()
        {
            Console.WriteLine("Please specify name:");
            string name = Console.ReadLine();
            Console.WriteLine("Please specifiy number (9 digits):");
            bool result = int.TryParse(Console.ReadLine(), out int number);
            while (!result || (99999999 > number || number > 999999999))
            {
                Console.Write("Specified number was incorrect.\nPlease specifiy correct number (9 digits):\n");
                result = int.TryParse(Console.ReadLine(), out number);
            }
            Tuple<string, int> contactDetails = new Tuple<string, int>(name, number);
            return contactDetails;
        }

        public void AddContact(List<PhoneNumber> numbers, Tuple<string, int> contactDetails)
        {
            numbers.Add(new PhoneNumber(contactDetails.Item1, contactDetails.Item2));
            Console.WriteLine($"New contact with\nName: {contactDetails.Item1}\nNumber: {contactDetails.Item2} is added to phone book.");
        }

        public void DeleteContact(List<PhoneNumber> numbers, string number)
        {
            var contact = FindContact(numbers, number);
            if (contact != null)
            {
                Console.WriteLine($"Contact {contact.Name} is removed from phone book.");
                numbers.Remove(contact);
            }
        }

        public void AddNote(List<PhoneNumber> numbers, string number)
        {
            var contact = FindContact(numbers, number);
            if (contact != null)
            {
                Console.WriteLine("Please specifiy note which you want to add:");
                contact.Note = Console.ReadLine();
                Console.WriteLine("Note is added to contact.");
            }
            Console.ReadKey();
        }
    }
}
