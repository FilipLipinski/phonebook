﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhoneBook;


namespace PhoneBookTests
{
    [TestClass]
    public class PhoneBookTests1
    {
        private BookMethods bookMethods = new BookMethods(); 

        [TestMethod]
        public void NonExistingContactWillNotBeFound()
        {
            List<PhoneNumber> numbers = new List<PhoneNumber>(){new PhoneNumber(), new PhoneNumber(
                "Jack Sparrow", 111222333)};
            PhoneNumber num =  bookMethods.FindContact(numbers, "Tom Riddle".ToLower());
            Assert.IsNull(num);
        }

        [TestMethod]
        public void ExistingContactIsFound()
        {
            List<PhoneNumber> numbers = new List<PhoneNumber>(){new PhoneNumber(), new PhoneNumber(
                "Jack Sparrow", 111222333)};
            PhoneNumber num = bookMethods.FindContact(numbers, "Jack Sparrow".ToLower());
            Assert.IsNotNull(num);
        }

        [TestMethod]
        public void RemovedContactWillNotBeFound()
        {
            List<PhoneNumber> numbers = new List<PhoneNumber>(){new PhoneNumber(), new PhoneNumber(
                "Jack Sparrow", 111222333)};
            bookMethods.DeleteContact(numbers, "Jack Sparrow".ToLower());
            Assert.IsTrue(numbers.Count == 1);
        }

        [TestMethod]
        public void NonMatchingContactWillNotRemoveOtherContacts()
        {
            List<PhoneNumber> numbers = new List<PhoneNumber>(){new PhoneNumber(), new PhoneNumber(
                "Jack Sparrow", 111222333)};
            bookMethods.DeleteContact(numbers, "Tom Riddle".ToLower());
            Assert.IsTrue(numbers.Count == 2);
        }

        [TestMethod]
        public void NewContactWillBeFoundInPhoneBook()
        {
            List<PhoneNumber> numbers = new List<PhoneNumber>(){new PhoneNumber(), new PhoneNumber(
                "Jack Sparrow", 111222333)};
            Tuple<string, int> newContact = new Tuple<string, int>("Britney Spears", 555777555);
            bookMethods.AddContact(numbers, newContact);
            Assert.IsTrue(numbers[2].Name == "Britney Spears" && numbers[2].Number == 555777555);
        }
    }
}
