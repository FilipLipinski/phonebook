﻿using System;
using System.Collections.Generic;

namespace PhoneBook
{
    class Program
    {
        static void Main()
        {
            List<PhoneNumber> contactsList = new List<PhoneNumber>(){new PhoneNumber()};
            BookMethods bookMethods = new BookMethods();
            string command = "";

            while (command != "exit")
            {
                command = MenuInit();

                if (command == "list") bookMethods.ListContacts(contactsList);
                else if (command == "find") bookMethods.FindAndPrintContact(contactsList, GetName());
                else if (command == "add")
                {
                    Tuple<string, int> contacTuple = bookMethods.AddContactValidator();
                    bookMethods.AddContact(contactsList, contacTuple);
                    Console.ReadKey();
                }
                else if (command == "delete")
                {
                    bookMethods.DeleteContact(contactsList, GetName());
                    Console.ReadKey();
                }
                else if (command == "note") bookMethods.AddNote(contactsList, GetName());
                else if (command == "help") Console.Clear();
                else if (command == "exit") { }
                else
                {
                    Console.WriteLine("Given command is not recognized. Please use correct command...");
                    Console.ReadKey();
                }
            } 
        }

        private static string MenuInit()
        {
            Console.Clear();
            Console.WriteLine("List of available commands:" +
                              "\n\"Add\" will add new contact to phone book." +
                              "\n\"Delete\" will remove contact from phone book." +
                              "\n\"List\" will list all available contacts." +
                              "\n\"Find\" will find contact in phone book." +
                              "\n\"Note\" will add note to existing contact." +
                              "\n\"Help\" will clear console and display available commands." +
                              "\n\"Exit\" will close program.");
            return Console.ReadLine()?.ToLower();
        }

        private static string GetName()
        {
            Console.WriteLine("Please specify name connected with contact:");
            return Console.ReadLine().ToLower();
        }

    }
}
