﻿
namespace PhoneBook
{
    public class PhoneNumber
    {
        ///<summary>default constructor</summary>
        public PhoneNumber()
        {
            Name = "John Wallace";
            Number = 123456789;
        }

        ///<summary>add phone number without note</summary>
        public PhoneNumber(string name, int number)
        {
            Name = name;
            Number = number;
        }

        ///<summary>add phone number with note</summary>
        public PhoneNumber(string name, int number, string note)
        {
            Name = name;
            Number = number;
            Note = note;
        }

        public string Name { get; set; }
        public int Number { get; set; }
        public string Note { get; set; }

        
    }
}
